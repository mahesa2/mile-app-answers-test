// terdapat 2 variabel A & B
A = 3
B = 5

// Tukar Nilai variabel A dan B, Syarat Tidak boleh menambah Variabel Baru

// Hasil yang diharapkan :
A = 5
B = 3

A = A + B;
B = A - B;
A = A - B;

console.log(A, B);